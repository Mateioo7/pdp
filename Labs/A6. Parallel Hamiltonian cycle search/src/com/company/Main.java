package com.company;

import com.company.model.DirectedGraph;
import com.company.model.threads.CycleSearchTask;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class Main {
    private static final int VERTICES_COUNT = 1000;
    private static final int THREADS_COUNT = 2;

    public static void main(String[] args) throws InterruptedException {
        DirectedGraph graph = DirectedGraph.generateRandomHamiltonian(VERTICES_COUNT);

        float startTime = System.nanoTime() / 1_000_000f;
        threadedSearch(graph);
        float endTime = System.nanoTime() / 1_000_000f;
        float duration = (endTime - startTime) / 1000;
        System.out.println(VERTICES_COUNT + " vertices: " + duration + " seconds");
    }

    private static void threadedSearch(DirectedGraph graph) throws InterruptedException {
        ExecutorService pool = Executors.newFixedThreadPool(THREADS_COUNT);

        List<Integer> result = new ArrayList<>(graph.size());
        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        for (int i = 0; i < graph.size(); i++) {
            pool.submit(new CycleSearchTask(graph, i, result, atomicBoolean));
        }

        pool.shutdown();
        pool.awaitTermination(10, TimeUnit.SECONDS);
    }
}
