package com.company.model.threads;

import com.company.model.DirectedGraph;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CycleSearchTask implements Runnable {
    private final DirectedGraph graph;
    private final int startingNode;
    private final List<Integer> path;
    private final Lock lock;
    private final List<Integer> result;
    private final AtomicBoolean cycleFound;

    public CycleSearchTask(DirectedGraph graph, int node, List<Integer> result, AtomicBoolean cycleFound) {
        this.graph = graph;
        this.startingNode = node;
        this.path = new ArrayList<>();
        this.lock = new ReentrantLock();
        this.cycleFound = cycleFound;
        this.result = result;
    }

    @Override
    public void run() {
        visit(startingNode);
    }

    private void visit(int node) {
        path.add(node);
        if (!cycleFound.get()) {
            if (path.size() == graph.size()) {
                if (graph.neighboursOf(node).contains(startingNode)) {
                    cycleFound.set(true);
                    this.lock.lock();
                    result.clear();
                    result.addAll(this.path);
                    this.lock.unlock();
                }
                return;
            }

            graph.neighboursOf(node).forEach(neighbour -> {
                if (!this.path.contains(neighbour)) {
                    visit(neighbour);
                }
            });
        }
    }
}