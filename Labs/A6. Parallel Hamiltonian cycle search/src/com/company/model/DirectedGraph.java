package com.company.model;

import java.util.*;

import static java.util.Collections.shuffle;

public class DirectedGraph {
    private final Map<Integer, List<Integer>> edges = new HashMap<>();

    public DirectedGraph(int nodeCount) {
        for (int i = 0; i < nodeCount; i++) {
            edges.put(i, new ArrayList<>());
        }
    }

    public void addEdge(int nodeA, int nodeB) {
        edges.get(nodeA).add(nodeB);
    }

    public List<Integer> neighboursOf(int node) {
        return edges.get(node);
    }

    public List<Integer> getNodes() {
        return new ArrayList<>(edges.keySet());
    }

    public int size() {
        return edges.size();
    }

    public static DirectedGraph generateRandomHamiltonian(int size) {
        DirectedGraph graph = new DirectedGraph(size);
        List<Integer> nodes = graph.getNodes();
        shuffle(nodes);

        for (int i = 1; i < nodes.size(); i++) {
            graph.addEdge(nodes.get(i - 1), nodes.get(i));
        }
        // add cycle
        graph.addEdge(nodes.get(nodes.size() - 1), nodes.get(0));

        Random random = new Random();
        for (int i = 0; i < size / 2; i++) {
            int nodeA = random.nextInt(size - 1);
            int nodeB = random.nextInt(size - 1);
            graph.addEdge(nodeA, nodeB);
        }

        return graph;
    }
}