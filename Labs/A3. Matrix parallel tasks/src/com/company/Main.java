package com.company;

import com.company.model.Matrix;
import com.company.model.runner.NormalThreadRunner;
import com.company.model.runner.ThreadPoolRunner;

public class Main {
    public static final String APPROACH = "pool";
    public static final String THREAD_TYPE = "kth";
    private static final int ROW_COUNT = 100;
    private static final int COLUMN_COUNT = 100;
    private static final int THREADS_COUNT = 4;

    public static void main(String[] args) {
        Matrix matrix1 = new Matrix(ROW_COUNT, COLUMN_COUNT);
        Matrix matrix2 = new Matrix(ROW_COUNT, COLUMN_COUNT);

        matrix1.populate();
        matrix2.populate();

        System.out.println("First matrix:\n" + matrix1);
        System.out.println("Second matrix:\n" + matrix2);

        Matrix productMatrix = new Matrix(ROW_COUNT, COLUMN_COUNT);

        float start = System.nanoTime() / 1_000_000f;

        if (APPROACH.equals("pool")) {
            ThreadPoolRunner.run(THREADS_COUNT, THREAD_TYPE, matrix1, matrix2, productMatrix);
        } else if (APPROACH.equals("normal")) {
            NormalThreadRunner.run(THREADS_COUNT, THREAD_TYPE, matrix1, matrix2, productMatrix);
        }

        float end = System.nanoTime() / 1_000_000f;
        System.out.println("Time elapsed: " + (end - start) / 1000 + " seconds");
    }
}
