package com.company.model;

public class Pair<K, V> {
    public final K first;
    public final V second;

    private Pair(K first, V second) {
        this.first = first;
        this.second = second;
    }

    public static <K, V> Pair<K, V> of(K a, V b) {
        return new Pair<>(a, b);
    }

    @Override
    public String toString() {
        return "(" + first + ", " + second + ")";
    }
}