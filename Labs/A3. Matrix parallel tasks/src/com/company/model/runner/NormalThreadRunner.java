package com.company.model.runner;

import com.company.model.Matrix;
import com.company.utils.MatrixUtils;

import java.util.ArrayList;
import java.util.List;

public class NormalThreadRunner {
    public static void run(int threadsCount, String threadType, Matrix matrix1, Matrix matrix2, Matrix productMatrix) {
        List<Thread> threads = new ArrayList<>();

        switch (threadType) {
            case "row":
                for (int threadIndex = 0; threadIndex < threadsCount; threadIndex++) {
                    threads.add(MatrixUtils.createRowThreadTask(threadsCount, threadIndex, matrix1, matrix2, productMatrix));
                }
                break;
            case "column":
                for (int threadIndex = 0; threadIndex < threadsCount; threadIndex++) {
                    threads.add(MatrixUtils.createColumnThreadTask(threadsCount, threadIndex, matrix1, matrix2, productMatrix));
                }
                break;
            case "kth":
                for (int threadIndex = 0; threadIndex < threadsCount; threadIndex++) {
                    threads.add(MatrixUtils.createKthThreadTask(threadsCount, threadIndex, matrix1, matrix2, productMatrix));
                }
                break;
        }

        for (Thread thread : threads) {
            thread.start();
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Product matrix:\n" + productMatrix.toString());
    }
}
