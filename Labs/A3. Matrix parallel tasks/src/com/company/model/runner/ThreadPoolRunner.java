package com.company.model.runner;

import com.company.exceptions.MatrixException;
import com.company.model.Matrix;
import com.company.utils.MatrixUtils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public final class ThreadPoolRunner {
    public static void run(int threadsCount, String threadType, Matrix matrix1, Matrix matrix2, Matrix productMatrix) throws MatrixException {
        ExecutorService service = Executors.newFixedThreadPool(threadsCount);

        switch (threadType) {
            case "row":
                for (int threadIndex = 0; threadIndex < threadsCount; threadIndex++) {
                    service.submit(MatrixUtils.createRowThreadTask(threadsCount, threadIndex, matrix1, matrix2, productMatrix));
                }
                break;
            case "column":
                for (int threadIndex = 0; threadIndex < threadsCount; threadIndex++) {
                    service.submit(MatrixUtils.createColumnThreadTask(threadsCount, threadIndex, matrix1, matrix2, productMatrix));
                }
                break;
            case "kth":
                for (int threadIndex = 0; threadIndex < threadsCount; threadIndex++) {
                    service.submit(MatrixUtils.createKthThreadTask(threadsCount, threadIndex, matrix1, matrix2, productMatrix));
                }
                break;
        }

        service.shutdown();
        try {
            if (!service.awaitTermination(300, TimeUnit.SECONDS)) {
                service.shutdownNow();
            }

            System.out.println("Product matrix:\n" + productMatrix.toString());
        } catch (InterruptedException ex) {
            service.shutdownNow();
            ex.printStackTrace();
        }
    }
}