package com.company.model.threads;

import com.company.model.Matrix;
import com.company.model.Pair;

public class RowTask extends MatrixTask {
    public RowTask(int rowStart, int columnStart, int count, Matrix matrix1, Matrix matrix2, Matrix productMatrix) {
        super(rowStart, columnStart, count, matrix1, matrix2, productMatrix);
    }

    public void computeProductPositions() {
        int rowIndex = rowStartIndex, columnIndex = columnStartIndex;
        int positionsCount = super.positionsCount;
        while (positionsCount > 0 && rowIndex < productMatrix.rowCount && columnIndex < productMatrix.columnCount) {
            productPositions.add(Pair.of(rowIndex, columnIndex));
            columnIndex++;
            positionsCount--;
            if (columnIndex == productMatrix.columnCount) {
                columnIndex = 0;
                rowIndex++;
            }
        }
    }
}