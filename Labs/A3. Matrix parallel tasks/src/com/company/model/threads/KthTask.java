package com.company.model.threads;

import com.company.model.Matrix;
import com.company.model.Pair;

public class KthTask extends MatrixTask {
    public KthTask(int rowStart, int columnStart, int count, Matrix matrix1, Matrix matrix2, Matrix productMatrix, int k) {
        super(rowStart, columnStart, count, matrix1, matrix2, productMatrix, k);
    }

    public void computeProductPositions() {
        int rowIndex = rowStartIndex, columnIndex = columnStartIndex;
        int positionsCount = super.positionsCount;
        while (positionsCount > 0 && rowIndex < productMatrix.rowCount) {
            productPositions.add(Pair.of(rowIndex, columnIndex));
            positionsCount--;
            rowIndex += (columnIndex + k) / productMatrix.columnCount;
            columnIndex = (columnIndex + k) % productMatrix.columnCount;
        }
    }
}