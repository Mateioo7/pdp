package com.company.model.threads;

import com.company.model.Matrix;
import com.company.model.Pair;
import com.company.utils.MatrixUtils;

import java.util.ArrayList;
import java.util.List;

public abstract class MatrixTask extends Thread {
    public final int rowStartIndex, columnStartIndex, positionsCount;
    public final Matrix matrix1, matrix2, productMatrix;
    public List<Pair<Integer, Integer>> productPositions;
    public int k;

    public MatrixTask(int rowStartIndex, int columnStartIndex, int positionsCount,
                      Matrix matrix1, Matrix matrix2, Matrix productMatrix, int k) {
        this.rowStartIndex = rowStartIndex;
        this.columnStartIndex = columnStartIndex;
        this.positionsCount = positionsCount;
        this.matrix1 = matrix1;
        this.matrix2 = matrix2;
        this.productMatrix = productMatrix;
        this.k = k;
        this.productPositions = new ArrayList<>();
        computeProductPositions();
    }

    public MatrixTask(int rowStartIndex, int columnStartIndex, int positionsCount,
                      Matrix matrix1, Matrix matrix2, Matrix productMatrix) {
        this.rowStartIndex = rowStartIndex;
        this.columnStartIndex = columnStartIndex;
        this.positionsCount = positionsCount;
        this.matrix1 = matrix1;
        this.matrix2 = matrix2;
        this.productMatrix = productMatrix;
        this.productPositions = new ArrayList<>();
        computeProductPositions();
    }

    public abstract void computeProductPositions();

    @Override
    public void run() {
        for (Pair<Integer, Integer> p : productPositions) {
            int rowIndex = p.first;
            int columnIndex = p.second;
            try {
                productMatrix.set(rowIndex, columnIndex,
                        MatrixUtils.computeProduct(matrix1, rowIndex, matrix2, columnIndex));
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
        }
    }
}