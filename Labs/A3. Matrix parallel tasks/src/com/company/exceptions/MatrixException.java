package com.company.exceptions;

public class MatrixException extends RuntimeException {
    public MatrixException(String message) {
        super(message);
    }
}
