package com.company.utils;

import com.company.exceptions.MatrixException;
import com.company.model.Matrix;
import com.company.model.threads.ColumnTask;
import com.company.model.threads.KthTask;
import com.company.model.threads.MatrixTask;
import com.company.model.threads.RowTask;

public class MatrixUtils {
    public static int computeProduct(Matrix matrix1, int rowIndex, Matrix matrix2, int columnIndex) throws RuntimeException {
        if (rowIndex < matrix1.rowCount && columnIndex < matrix2.columnCount) {
            int product = 0;
            for (int i = 0; i < matrix1.columnCount; i++) {
                product += matrix1.get(rowIndex, i) * matrix2.get(i, columnIndex);
            }
            return product;
        } else {
            throw new RuntimeException("Row or column index out of bounds!");
        }
    }

    public static MatrixTask createRowThreadTask(int threadsCount, int threadIndex,
                                                 Matrix matrix1, Matrix matrix2, Matrix productMatrix) {
        int totalPositionsCount = productMatrix.rowCount * productMatrix.columnCount;
        int taskPositionsCount = totalPositionsCount / threadsCount;

        int rowStartIndex = taskPositionsCount * threadIndex / productMatrix.rowCount;
        int columnStartIndex = taskPositionsCount * threadIndex % productMatrix.columnCount;

        // if it's the last thread, also get the remaining positions
        if (threadIndex == threadsCount - 1) {
            taskPositionsCount += totalPositionsCount % threadsCount;
        }

        return new RowTask(rowStartIndex, columnStartIndex, taskPositionsCount, matrix1, matrix2, productMatrix);
    }

    public static MatrixTask createColumnThreadTask(int threadsCount, int threadIndex,
                                                    Matrix matrix1, Matrix matrix2, Matrix productMatrix) {
        int totalPositionsCount = productMatrix.rowCount * productMatrix.columnCount;
        int taskPositionsCount = totalPositionsCount / threadsCount;

        int rowStartIndex = taskPositionsCount * threadIndex % productMatrix.rowCount;
        int columnStartIndex = taskPositionsCount * threadIndex / productMatrix.columnCount;

        // if it's the last thread, also get the remaining positions
        if (threadIndex == threadsCount - 1) {
            taskPositionsCount += totalPositionsCount % threadsCount;
        }

        return new ColumnTask(rowStartIndex, columnStartIndex, taskPositionsCount, matrix1, matrix2, productMatrix);
    }

    public static MatrixTask createKthThreadTask(int threadsCount, int threadIndex,
                                                 Matrix matrix1, Matrix matrix2, Matrix productMatrix) {
        int totalPositionsCount = productMatrix.rowCount * productMatrix.columnCount;
        int k = threadsCount;

        if (totalPositionsCount % k != 0) {
            throw new MatrixException("Matrix elements count " + totalPositionsCount +
                    " not divisible with threads count (k) " + threadsCount);
        }

        int taskPositionsCount = totalPositionsCount / threadsCount;

        int rowStartIndex = threadIndex / productMatrix.columnCount;
        int columnStartIndex = threadIndex % productMatrix.columnCount;

        return new KthTask(rowStartIndex, columnStartIndex, taskPositionsCount, matrix1, matrix2, productMatrix, k);
    }
}
