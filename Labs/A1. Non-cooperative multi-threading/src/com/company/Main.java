package com.company;

import com.company.service.BankService;

public class Main {
    public static void main(String[] args) {
        BankService bankService = new BankService(100, 200000, 5);
        bankService.runTransfersMultiThreaded();
    }
}
