package com.company.utils;

import java.util.concurrent.ThreadLocalRandom;

public class NumberUtils {
    public static int getRandomInt(int origin, int bound) {
        return ThreadLocalRandom.current().nextInt(origin, bound);
    }
}
