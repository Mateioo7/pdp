/*
package com.company.utils;

import com.company.model.BankAccount;

class Scratch {
    public boolean transfer(BankAccount destinationAccount, int amount) {
        if (amount > currentBalance) {
            return false;
        }
        // this.id = 2, destinationAccount.id = 1

        // Deadlock-free code
        if (this.getId() < destinationAccount.getId()) {
            this.mutex.lock();
            destinationAccount.mutex.lock();
        } else {
            // 1 lock - thread1 / thread2 will lock and thread2 / thread1 will wait
            destinationAccount.mutex.lock();
            this.mutex.lock();
        }

        // Deadlock code
        // 2 lock
        this.mutex.lock();

        // 1 lock - unavailable since scratch1 already locked 1 -> deadlock!!
        destinationAccount.mutex.lock();

        // do stuff

        // 2, 1 unlock
        this.mutex.unlock();
        destinationAccount.mutex.unlock();

        return true;
    }
}
*/