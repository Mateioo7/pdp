/*
package com.company.utils;

import com.company.model.BankAccount;

class Scratch {
    public boolean transfer(BankAccount destinationAccount, int amount) {
        if (amount > currentBalance) {
            return false;
        }
        // this.id = 1, destinationAccount.id = 2

        // Deadlock-free code
        if (this.id) < destinationAccount.id) {
            // 1 lock - thread1 / thread2 will lock and thread2 / thread1 will wait
            this.mutex.lock();
            destinationAccount.mutex.lock();
        } else {
            destinationAccount.mutex.lock();
            this.mutex.lock();
        }

        // Deadlock code
        // 1 lock
        this.mutex.lock();

        // 2 lock - unavailable since scratch2 already locked 2 -> deadlock!!
        destinationAccount.mutex.lock();

        // do stuff

        // 1, 2 unlock
        this.mutex.unlock();
        destinationAccount.mutex.unlock();

        return true;
    }
}
*/