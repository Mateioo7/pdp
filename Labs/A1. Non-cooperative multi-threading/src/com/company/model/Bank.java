package com.company.model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Bank {
    private final List<BankAccount> bankAccounts;

    public Bank() {
        bankAccounts = new ArrayList<>();
    }

    public List<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    public void checkCorrectness() {
        System.out.println("Checking correctness ...");

        AtomicInteger incorrectAccounts = new AtomicInteger(0);
        for (BankAccount bankAccount : this.bankAccounts) {
            bankAccount.getMutex().lock();

            if (!bankAccount.hasCorrectData()) {
                incorrectAccounts.getAndIncrement();
            }

            bankAccount.getMutex().unlock();
        }

        if (incorrectAccounts.get() > 0) {
            System.err.println("Consistency check failed.");
        }
    }

    @Override
    public String toString() {
        return "Bank{" +
                "bankAccounts=" + bankAccounts +
                '}';
    }
}
