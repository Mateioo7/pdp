package com.company.model;

public class Transfer {
    private final int sourceAccountId;
    private final int destinationAccountId;
    private final int amount;
    private final long timestamp;

    public Transfer(int sourceAccountId, int destinationAccountId, int amount, long timestamp) {
        this.sourceAccountId = sourceAccountId;
        this.destinationAccountId = destinationAccountId;
        this.amount = amount;
        this.timestamp = timestamp;
    }

    public int getSourceAccountId() {
        return sourceAccountId;
    }

    public int getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "Transfer{" +
                "sourceAccountId=" + sourceAccountId +
                ", destinationAccountId=" + destinationAccountId +
                ", amount=" + amount +
                ", timestamp=" + timestamp +
                '}';
    }
}
