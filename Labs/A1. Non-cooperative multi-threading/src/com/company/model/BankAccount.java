package com.company.model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BankAccount {
    private final int id;
    private final int initialBalance;
    private int currentBalance;

    private final List<Transfer> transferLog;
    private final Lock mutex;

    public BankAccount(int id, int initialBalance) {
        this.id = id;
        this.initialBalance = initialBalance;
        this.currentBalance = initialBalance;
        this.transferLog = new ArrayList<>();
        this.mutex = new ReentrantLock();
    }

    public void transfer(BankAccount destinationAccount, int amount) {
        // declare an order of locking to prevent deadlock - see utils.thread1 & utils.thread2 package for explanation
        if (this.id < destinationAccount.id) {
            this.mutex.lock();
            destinationAccount.mutex.lock();
        } else {
            destinationAccount.mutex.lock();
            this.mutex.lock();
        }

        if (amount > this.currentBalance) {
            this.mutex.unlock();
            destinationAccount.mutex.unlock();
            return;
        }

        this.currentBalance -= amount;
        destinationAccount.currentBalance += amount;

        long timestamp = System.currentTimeMillis();
        Transfer transfer = new Transfer(this.id, destinationAccount.id, amount, timestamp);
        this.transferLog.add(transfer);
        destinationAccount.transferLog.add(transfer);

        this.mutex.unlock();
        destinationAccount.mutex.unlock();
    }

    public boolean hasCorrectData() {
        int balance = this.initialBalance;
        for (Transfer transfer : this.transferLog) {
            if (transfer.getSourceAccountId() == this.id) {
                balance -= transfer.getAmount();
            } else {
                balance += transfer.getAmount();
            }
        }

        return balance == this.currentBalance;
    }

    public int getCurrentBalance() {
        return currentBalance;
    }

    public Lock getMutex() {
        return mutex;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "id=" + id +
                ", initialBalance=" + initialBalance +
                ", currentBalance=" + currentBalance +
                ", transferLog=" + transferLog +
                '}';
    }
}
