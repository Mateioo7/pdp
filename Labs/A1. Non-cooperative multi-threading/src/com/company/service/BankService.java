package com.company.service;

import com.company.model.Bank;
import com.company.model.BankAccount;
import com.company.utils.NumberUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BankService {
    private final int ACCOUNTS_NUMBER;
    private final int THREADS_NUMBER;
    private final int TRANSFERS_PER_THREAD_NUMBER;

    private final Bank bank;
    private final Lock checkerMutex;
    private boolean stillChecking;

    public BankService(int accountsNumber, int transfersNumber, int threadsNumber) {
        ACCOUNTS_NUMBER = accountsNumber;
        THREADS_NUMBER = threadsNumber;
        TRANSFERS_PER_THREAD_NUMBER = transfersNumber / THREADS_NUMBER;

        this.bank = new Bank();
        this.checkerMutex = new ReentrantLock();
        this.stillChecking = true;
    }

    public void runTransfersMultiThreaded() {
        this.createBankAccounts();
        // to get milliseconds
        float startTime = System.nanoTime() / 1000000f;

        List<Thread> threads = this.createThreads();
        threads.forEach(thread -> {
            this.checkerMutex.lock();
            thread.start();
            this.checkerMutex.unlock();
        });

        Thread correctnessCheckerThread = new Thread(() -> {
            while (stillChecking) {
                // 1/10 probability
                if (NumberUtils.getRandomInt(0, 10) == 0) {
                    this.checkerMutex.lock();
                    this.bank.checkCorrectness();
                    this.checkerMutex.unlock();
                }
            }
        });
        correctnessCheckerThread.start();

        threads.forEach(thread -> {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        this.checkerMutex.lock();
        stillChecking = false;
        this.checkerMutex.unlock();

        try {
            correctnessCheckerThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        this.bank.checkCorrectness();

        float endTime = System.nanoTime() / 1000000f;
        System.out.println("Time elapsed: " + (endTime - startTime) / 1000 + " seconds");
    }

    private List<Thread> createThreads() {
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < THREADS_NUMBER; i++) {
            threads.add(new Thread(() -> {
                for (int j = 0; j < TRANSFERS_PER_THREAD_NUMBER; j++) {
                    int sourceAccountId = NumberUtils.getRandomInt(0, ACCOUNTS_NUMBER);
                    int destinationAccountId = NumberUtils.getRandomInt(0, ACCOUNTS_NUMBER);

                    if (sourceAccountId == destinationAccountId) {
                        j--;
                        continue;
                    }

                    BankAccount sourceAccount = bank.getBankAccounts().get(sourceAccountId);
                    BankAccount destinationAccount = bank.getBankAccounts().get(destinationAccountId);

                    int amount = NumberUtils.getRandomInt(0, sourceAccount.getCurrentBalance() + 1);
                    sourceAccount.transfer(destinationAccount, amount);
                }
            }));
        }

        return threads;
    }

    private void createBankAccounts() {
        for (int id = 0; id < ACCOUNTS_NUMBER; id++) {
            bank.getBankAccounts().add(new BankAccount(id, 50));
        }
    }
}
