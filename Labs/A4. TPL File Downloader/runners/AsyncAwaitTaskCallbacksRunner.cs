﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using A4.TPL_File_Downloader.model;
using A4.TPL_File_Downloader.utils;

namespace A4.TPL_File_Downloader.tasks {
    internal class AsyncAwaitTaskCallbacksRunner {
        private static List<string> hosts;

        public static void Execute(List<string> hostnames) {
            hosts = hostnames;
            List<Task> tasks = new List<Task>();

            for (int i = 0; i < hosts.Count; i++) {
                tasks.Add(Task.Factory.StartNew(RunTask, i));
            }

            Task.WaitAll(tasks.ToArray());
        }

        private static async void RunTask(object id) {
            int intId = (int)id;
            // e.g: host = www.cs.ubbcluj.ro/~rlupsa/edu/pdp
            // domain name = www.cs.ubbcluj.ro
            // endpoint = /~rlupsa/edu/pdp
            string domainName = hosts[intId].Split('/')[0];
            IPHostEntry hostInfo = Dns.GetHostEntry(domainName);
            IPAddress address = hostInfo.AddressList[0];

            Socket socket = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            SocketState socketState = new SocketState(
                socket,
                intId,
                domainName,
                "/" + hosts[intId].Split('/')[1],
                new IPEndPoint(address, 80));

            await Connect(socketState);
            await Send(socketState);
            await Receive(socketState);

            socketState.socket.Close();
        }

        private static async Task Connect(SocketState socketState) {
            socketState.socket.BeginConnect(socketState.hostAddress, ConnectCallback, socketState);
            await Task.FromResult(socketState.beginConnectFinished.WaitOne());
        }

        private static void ConnectCallback(IAsyncResult state) {
            SocketState socketState = (SocketState)state.AsyncState;

            socketState.socket.EndConnect(state);

            Console.WriteLine("Connection {0} > Socket BeginConnect finished at {1} ({2})",
                socketState.id, socketState.domainName, socketState.hostAddress);
            // send signal to proceed
            socketState.beginConnectFinished.Set();
        }

        private static async Task Send(SocketState socketState) {
            byte[] buffer = Encoding.ASCII.GetBytes(Parser.GetRequestHeader(socketState.domainName, socketState.endpoint));
            socketState.socket.BeginSend(buffer, 0, buffer.Length, 0, SendCallback, socketState);
            await Task.FromResult(socketState.beginSendFinished.WaitOne());
        }

        private static void SendCallback(IAsyncResult state) {
            SocketState socketState = (SocketState)state.AsyncState;

            int bytesSentCount = socketState.socket.EndSend(state);

            Console.WriteLine("Connection {0} > Socket BeginSend finished with {1} bytes sent",
                socketState.id, bytesSentCount);
            // send signal to proceed
            socketState.beginSendFinished.Set();
        }

        private static async Task Receive(SocketState socketState) {
            socketState.socket.BeginReceive(socketState.responseBuffer, 0, socketState.BUFFER_SIZE, 0, ReceiveCallback, socketState);
            await Task.FromResult(socketState.beginReceiveFinished.WaitOne());
        }

        private static void ReceiveCallback(IAsyncResult state) {
            SocketState socketState = (SocketState)state.AsyncState;

            int bytesReceivedCount = socketState.socket.EndReceive(state);

            Console.WriteLine("Connection {0} > Socket BeginReceive finished with {1} bytes received",
                socketState.id, bytesReceivedCount);

            String stringResponse = Encoding.ASCII.GetString(socketState.responseBuffer, 0, bytesReceivedCount);
            Console.WriteLine("Connection {0} > Response content:\n {1}", socketState.id, stringResponse);

            Console.WriteLine("Connection {0} > Response Content-Length header value: {1}",
                socketState.id, Parser.GetContentLengthHeaderValue(stringResponse));
            // send signal to proceed
            socketState.beginSendFinished.Set();
        }
    }
}
