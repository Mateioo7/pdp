﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A4.TPL_File_Downloader.utils {
    internal class Parser {
        public static string GetRequestHeader(string domainName, string endpoint) {
            return "GET " + endpoint + " HTTP/1.1\r\n" +
                   "Host: " + domainName + "\r\n" +
                   "Content-Length: 0\r\n\r\n";
        }

        public static int GetContentLengthHeaderValue(string responseContent) {
            var contentLen = -1;
            var responseLines = responseContent.Split('\r', '\n');
            foreach (string responseLine in responseLines) {
                var lineDetails = responseLine.Split(':');

                if (lineDetails[0] == "Content-Length") {
                    return int.Parse(lineDetails[1]);
                }
            }
            return contentLen;
        }
    }
}
