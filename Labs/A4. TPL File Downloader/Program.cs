﻿using A4.TPL_File_Downloader.tasks;
using System.Collections.Generic;
using System.Linq;

namespace A4.TPL_File_Downloader
{
    internal class Program
    {
        static void Main(string[] args) {
            List<string> hosts = new string[] {
                "www.cs.ubbcluj.ro/~rlupsa/edu/pdp/", 
                "www.cs.ubbcluj.ro/~forest",
                "www.imdb.com/title/tt1243974/",
            }.ToList();

            //EventDrivenCallbacksRunner.Execute(hosts);
            //TaskCallbacksRunner.Execute(hosts);
            AsyncAwaitTaskCallbacksRunner.Execute(hosts);
        }
    }
}
