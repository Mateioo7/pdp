﻿using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace A4.TPL_File_Downloader.model
{
    internal class SocketState {
        public Socket socket = null;

        // event atomic variables
        public ManualResetEvent beginConnectFinished = new ManualResetEvent(false);
        public ManualResetEvent beginSendFinished = new ManualResetEvent(false);
        public ManualResetEvent beginReceiveFinished = new ManualResetEvent(false);

        public int BUFFER_SIZE = 1024;
        public byte[] responseBuffer;

        public int id;
        public string domainName;
        public string endpoint;

        // endpoint as an IP address and port number
        public IPEndPoint hostAddress;

        public SocketState(Socket socket, int id, string domainName, string endpoint, IPEndPoint hostAddress) {
            this.socket = socket;
            this.id = id;
            this.domainName = domainName;
            this.endpoint = endpoint;
            this.hostAddress = hostAddress;
            responseBuffer = new byte[BUFFER_SIZE];
        }
    }
}
