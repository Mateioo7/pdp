package com.company;

import com.company.model.Polynomial;
import com.company.service.Operation;

import java.util.concurrent.ExecutionException;

public class Main {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        Polynomial p = new Polynomial(5);
        Polynomial q = new Polynomial(5);

        System.out.println("P: " + p);
        System.out.println("Q: " + q);
        System.out.println("\n");

        System.out.println("Result: " + simpleSequential(p, q) + "\n");
        System.out.println("Result: " + simpleThreaded(p, q) + "\n");

        System.out.println("Result: " + karatsubaSequential(p, q).toString() + "\n");
        System.out.println("Result: " + karatsubaThreaded(p, q).toString() + "\n");
    }

    private static Polynomial simpleSequential(Polynomial p, Polynomial q) {
        float startTime = System.nanoTime() / 1_000_000f;
        Polynomial result = Operation.simpleSequential(p, q);
        float endTime = System.nanoTime() / 1_000_000f;

        System.out.println("Simple sequential multiplication");
        System.out.println("Time elapsed: " + (endTime - startTime) / 1000 + " seconds");
        return result;
    }

    private static Polynomial simpleThreaded(Polynomial p, Polynomial q) throws InterruptedException {
        float startTime = System.nanoTime() / 1_000_000f;
        Polynomial result = Operation.simpleThreaded(p, q);
        float endTime = System.nanoTime() / 1_000_000f;

        System.out.println("Simple parallel multiplication");
        System.out.println("Time elapsed: " + (endTime - startTime) / 1000 + " seconds");
        return result;
    }

    private static Polynomial karatsubaSequential(Polynomial p, Polynomial q) {
        float startTime = System.nanoTime() / 1_000_000f;
        Polynomial result = Operation.karatsubaSequential(p, q);
        float endTime = System.nanoTime() / 1_000_000f;

        System.out.println("Karatsuba sequential multiplication");
        System.out.println("Time elapsed: " + (endTime - startTime) / 1000 + " seconds");
        return result;
    }

    private static Polynomial karatsubaThreaded(Polynomial p, Polynomial q) throws ExecutionException,
            InterruptedException {
        float startTime = System.nanoTime() / 1_000_000f;
        Polynomial result = Operation.karatsubaThreaded(p, q, 1);
        float endTime = System.nanoTime() / 1_000_000f;

        System.out.println("Karatsuba parallel multiplication");
        System.out.println("Time elapsed: " + (endTime - startTime) / 1000 + " seconds");
        return result;
    }
}