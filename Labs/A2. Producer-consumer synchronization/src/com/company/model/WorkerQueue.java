package com.company.model;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class WorkerQueue {
    private final Lock mutex = new ReentrantLock();
    private final Condition mutexCondition = mutex.newCondition();
    private final Queue<Integer> queue = new ArrayDeque<>();
    private boolean isClosed = false;

    public void enqueue(Integer value) {
        mutex.lock();

        queue.add(value);
        mutexCondition.signal();

        mutex.unlock();
    }

    public Integer dequeue() {
        mutex.lock();

        while (true) {
            if (!queue.isEmpty()) {
                Integer valueToReturn = queue.remove();
                mutex.unlock();

                return valueToReturn;
            }

            if (isClosed) {
                mutex.unlock();
                return null;
            }

            try {
                mutexCondition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void close() {
        mutex.lock();

        isClosed = true;
        mutexCondition.signalAll();

        mutex.unlock();
    }
}
