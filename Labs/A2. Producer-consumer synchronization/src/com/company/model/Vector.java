package com.company.model;

import java.util.List;

public class Vector {
    private final List<Integer> content;

    public Vector(List<Integer> content) {
        this.content = content;
    }

    public Integer getOne(int index) {
        return content.get(index);
    }

    public Integer getDimension() {
        return content.size();
    }

    public void addToContent(Integer value) {
        content.add(value);
    }
}
