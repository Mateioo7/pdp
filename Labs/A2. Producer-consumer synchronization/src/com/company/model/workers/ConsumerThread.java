package com.company.model.workers;

import com.company.model.WorkerQueue;

public final class ConsumerThread extends Thread {
    private final WorkerQueue queue;
    private final int dimension;
    private int scalarProductSum;

    public ConsumerThread(WorkerQueue queue, int dimension) {
        super("Consumer");
        this.queue = queue;
        this.dimension = dimension;
        this.scalarProductSum = 0;
    }

    @Override
    public void run() {
        for (int i = 0; i < this.dimension; i++) {
            scalarProductSum += queue.dequeue();
            System.out.printf("Consumer: Scalar product sum is currently: %d %n", scalarProductSum);
        }

        System.out.printf("%nConsumer: Scalar product final sum is: %d", scalarProductSum);
    }
}