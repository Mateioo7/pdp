package com.company.model.workers;

import com.company.model.Vector;
import com.company.model.WorkerQueue;

public final class ProducerThread extends Thread {
    private final WorkerQueue queue;
    private final Vector vector1;
    private final Vector vector2;

    public ProducerThread(WorkerQueue queue, Vector vector1, Vector vector2) {
        super("Producer");
        this.queue = queue;
        this.vector1 = vector1;
        this.vector2 = vector2;
    }

    @Override
    public void run() {
        for (int i = 0; i < vector1.getDimension(); i++) {
            final int scalarProduct = vector1.getOne(i) * vector2.getOne(i);
            System.out.printf("Producer: enqueue %d * %d = %d %n", vector1.getOne(i), vector2.getOne(i), scalarProduct);
            queue.enqueue(scalarProduct);
        }

        System.out.println("Producer: Finished computing, closing queue");
        queue.close();
    }
}