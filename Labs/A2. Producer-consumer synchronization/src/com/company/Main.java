package com.company;

import com.company.model.Vector;
import com.company.model.WorkerQueue;
import com.company.model.workers.ConsumerThread;
import com.company.model.workers.ProducerThread;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        WorkerQueue queue = new WorkerQueue();

        final int n = 50;
        Vector vector = new Vector(new ArrayList<>());
        for (int i = 1; i <= n; i++) {
            vector.addToContent(i);
        }

        ProducerThread producer = new ProducerThread(queue, vector, vector);
        ConsumerThread consumer = new ConsumerThread(queue, vector.getDimension());

//        Vector vector1 = new Vector(Arrays.asList(1, 2, 3, 4));
//        Vector vector2 = new Vector(Arrays.asList(4, 3, 2, 1));
//
//        ProducerThread producer = new ProducerThread(queue, vector1, vector2);
//        ConsumerThread consumer = new ConsumerThread(queue, vector1.getDimension());

        producer.start();
        consumer.start();

        try {
            producer.join();
            consumer.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
