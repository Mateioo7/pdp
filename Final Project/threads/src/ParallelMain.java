import model.Pair;
import model.Puzzle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.logging.Logger;

public class ParallelMain {
    private static final Logger log = Logger.getLogger(String.valueOf(ParallelMain.class));

    private static final int NR_THREADS = 5;

    private static ExecutorService executorService;

    public static void main(String[] args) throws IOException, InterruptedException, ExecutionException {
        executorService = Executors.newFixedThreadPool(NR_THREADS);

        solve(Puzzle.fromFile());

        executorService.shutdown();
        executorService.awaitTermination(1000000, TimeUnit.SECONDS);
    }

    public static void solve(Puzzle inputPuzzle) throws ExecutionException, InterruptedException {
        long startTime = System.currentTimeMillis();

        int minimumSteps = inputPuzzle.getManhattanDistance();
        int currentMinimumSteps;
        while (true) {
            Pair<Integer, Puzzle> minStepsPuzzlePair = parallelSearch(inputPuzzle, 0, minimumSteps, NR_THREADS);
            currentMinimumSteps = minStepsPuzzlePair.getFirst();
            if (currentMinimumSteps == 0) {
                log.info("Solution found in " + minStepsPuzzlePair.getSecond().getStepsPerformed() + " steps");
                log.info("Execution time: " + (System.currentTimeMillis() - startTime) + "ms");
                log.info(minStepsPuzzlePair.getSecond().toStringWithHistory());
                return;
            }

            log.info(currentMinimumSteps + " minimum steps reached in " + (System.currentTimeMillis() - startTime) + "ms");
            minimumSteps = currentMinimumSteps;
        }
    }

    public static Pair<Integer, Puzzle> parallelSearch(Puzzle currentPuzzle, int stepsPerformed, int minimumSteps,
                                                       int nrThreads) throws ExecutionException, InterruptedException {
        if (nrThreads <= 1) {
            return basicSearch(currentPuzzle, stepsPerformed, minimumSteps);
        }

        int stepsEstimation = stepsPerformed + currentPuzzle.getManhattanDistance();
        if (stepsEstimation > minimumSteps) {
            return Pair.of(stepsEstimation, currentPuzzle);
        }
        if (currentPuzzle.getManhattanDistance() == 0) {
            return Pair.of(0, currentPuzzle);
        }

        int newMinimumSteps = Integer.MAX_VALUE;
        List<Puzzle> moves = currentPuzzle.generateValidMoves();
        List<Future<Pair<Integer, Puzzle>>> minStepsPuzzleResultPairs = new ArrayList<>();
        for (Puzzle next : moves) {
            Future<Pair<Integer, Puzzle>> minStepsPuzzlePairResult = executorService.submit(() -> parallelSearch(next, stepsPerformed + 1, minimumSteps, nrThreads / moves.size()));
            minStepsPuzzleResultPairs.add(minStepsPuzzlePairResult);
        }

        for (Future<Pair<Integer, Puzzle>> minStepsPuzzlePairResult : minStepsPuzzleResultPairs) {
            Pair<Integer, Puzzle> minStepsPuzzlePair = minStepsPuzzlePairResult.get();
            int currentMinimumSteps = minStepsPuzzlePair.getFirst();
            if (currentMinimumSteps == 0) {
                return Pair.of(0, minStepsPuzzlePair.getSecond());
            }

            if (currentMinimumSteps < newMinimumSteps) {
                newMinimumSteps = currentMinimumSteps;
            }
        }

        return Pair.of(newMinimumSteps, currentPuzzle);
    }

    public static Pair<Integer, Puzzle> basicSearch(Puzzle current, int stepsPerformed, int bound) {
        int stepsEstimation = stepsPerformed + current.getManhattanDistance();
        if (stepsEstimation > bound) {
            return Pair.of(stepsEstimation, current);
        }
        if (current.getManhattanDistance() == 0) {
            return Pair.of(0, current);
        }

        int newMinimumSteps = Integer.MAX_VALUE;
        Puzzle newPuzzle = null;
        for (Puzzle next : current.generateValidMoves()) {
            Pair<Integer, Puzzle> minStepsPuzzlePair = basicSearch(next, stepsPerformed + 1, bound);
            int minimumSteps = minStepsPuzzlePair.getFirst();
            if (minimumSteps == 0) {
                return Pair.of(0, minStepsPuzzlePair.getSecond());
            }
            if (minimumSteps < newMinimumSteps) {
                newMinimumSteps = minimumSteps;
                newPuzzle = minStepsPuzzlePair.getSecond();
            }
        }

        return Pair.of(newMinimumSteps, newPuzzle);
    }
}
