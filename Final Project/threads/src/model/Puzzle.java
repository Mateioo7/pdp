package model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

public class Puzzle implements Serializable {
    private final byte[][] tiles;
    private final Puzzle previousState;

    private final int freeXPosition;
    private final int freeYPosition;

    private final int stepsPerformed;
    private final int manhattanDistance;

    private static final int[] xDirections = new int[]{0, -1, 0, 1};
    private static final int[] yDirections = new int[]{-1, 0, 1, 0};
    private static final String[] moves = new String[]{"left", "up", "right", "down"};
    private final String move;

    public Puzzle(byte[][] tiles, int freeXPosition, int freeYPosition, int stepsPerformed, Puzzle previousState, String move) {
        this.tiles = tiles;
        this.freeXPosition = freeXPosition;
        this.freeYPosition = freeYPosition;
        this.stepsPerformed = stepsPerformed;
        this.previousState = previousState;
        this.move = move;
        this.manhattanDistance = computeManhattanDistance();
    }

    public static Puzzle fromFile() throws IOException {
        byte[][] tiles = new byte[4][4];
        int freeXPosition = -1, freeYPosition = -1;

        Scanner scanner = new Scanner(new BufferedReader(new FileReader("puzzle.in")));
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                tiles[i][j] = Integer.valueOf(scanner.nextInt()).byteValue();
                if (tiles[i][j] == 0) {
                    freeXPosition = i;
                    freeYPosition = j;
                }
            }
        }

        return new Puzzle(tiles, freeXPosition, freeYPosition, 0, null, "");
    }

    public int computeManhattanDistance() {
        int manhattanDistance = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (tiles[i][j] != 0) {
                    int stepsUntilTargetX = (tiles[i][j] - 1) / 4;
                    int stepsUntilTargetY = (tiles[i][j] - 1) % 4;
                    manhattanDistance += Math.abs(i - stepsUntilTargetX) + Math.abs(j - stepsUntilTargetY);
                }
            }
        }

        return manhattanDistance;
    }

    public List<Puzzle> generateValidMoves() {
        List<Puzzle> moves = new ArrayList<>();
        for (int k = 0; k < 4; k++) {
            if (freeXPosition + xDirections[k] >= 0 && freeXPosition + xDirections[k] < 4 && freeYPosition + yDirections[k] >= 0 && freeYPosition + yDirections[k] < 4) {
                int nextFreeXPosition = freeXPosition + xDirections[k];
                int nextFreeYPosition = freeYPosition + yDirections[k];
                if (previousState != null && nextFreeXPosition == previousState.freeXPosition && nextFreeYPosition == previousState.freeYPosition) {
                    continue;
                }

                byte[][] nextTiles = Arrays.stream(tiles).map(byte[]::clone).toArray(byte[][]::new);
                nextTiles[freeXPosition][freeYPosition] = nextTiles[nextFreeXPosition][nextFreeYPosition];
                nextTiles[nextFreeXPosition][nextFreeYPosition] = 0;

                moves.add(new Puzzle(nextTiles, nextFreeXPosition, nextFreeYPosition, stepsPerformed + 1, this, Puzzle.moves[k]));
            }
        }

        return moves;
    }

    public String toStringWithHistory() {
        Puzzle current = this;
        List<String> strings = new ArrayList<>();
        while (current != null) {
            StringBuilder stringBuilder = new StringBuilder("\n");
            stringBuilder.append("Free tile goes ").append(current.move).append(":\n");
            Arrays.stream(current.tiles).forEach(row -> stringBuilder.append(Arrays.toString(row)).append("\n"));

            strings.add(stringBuilder.toString());
            current = current.previousState;
        }
        Collections.reverse(strings);

        return String.join("", strings) + "after " + stepsPerformed + " steps performed";
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("\n");
        Arrays.stream(this.tiles).forEach(row -> stringBuilder.append(Arrays.toString(row)).append("\n"));
        stringBuilder.append("\n");

        return stringBuilder + "after " + this.stepsPerformed + " steps performed";
    }

    public int getStepsPerformed() {
        return stepsPerformed;
    }

    public int getManhattanDistance() {
        return manhattanDistance;
    }
}