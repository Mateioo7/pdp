import model.Pair;
import model.Puzzle;
import mpi.MPI;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Logger;

public class MpiMain {
    private static final Logger log = Logger.getLogger(String.valueOf(MpiMain.class));

    public static void main(String[] args) throws IOException {
        MPI.Init(args);

        int instanceId = MPI.COMM_WORLD.Rank();
        if (instanceId == 0) {
            masterLogic(Puzzle.fromFile());
        } else {
            workerLogic();
        }

        MPI.Finalize();
    }

    private static void masterLogic(Puzzle inputPuzzle) {
        log.info("Input puzzle: " + inputPuzzle);

        int instancesCount = MPI.COMM_WORLD.Size();
        log.info("Instances count: " + instancesCount);
        int workersCount = instancesCount - 1;
        log.info("Workers count: " + workersCount);
        int minimumSteps = inputPuzzle.getManhattanDistance();
        log.info("Minimum steps initially: " + minimumSteps);
        boolean solutionFound = false;
        long time = System.currentTimeMillis();

        // generate the starting configurations for the workers
        Queue<Puzzle> puzzlesQueue = new LinkedList<>();
        puzzlesQueue.add(inputPuzzle);

        while (true) {
            assert puzzlesQueue.peek() != null;
            // stop if we can't add all possible moves of the puzzle from the head of the queue
            if (!(puzzlesQueue.size() + puzzlesQueue.peek().generateValidMoves().size() - 1 <= workersCount)) {
                break;
            }

            Puzzle currentPuzzle = puzzlesQueue.poll();
            puzzlesQueue.addAll(currentPuzzle.generateValidMoves());
        }
        log.info("Generated " + puzzlesQueue.size() + " starting possible moves");

        while (!solutionFound) {
            Queue<Puzzle> puzzlesQueueCopy = new LinkedList<>(puzzlesQueue);
            for (int i = 0; i < puzzlesQueue.size(); i++) {
                // for each worker, send a starting possible move
                Puzzle currentPuzzle = puzzlesQueueCopy.poll();
                MPI.COMM_WORLD.Send(new boolean[]{false}, 0, 1, MPI.BOOLEAN, i + 1, 0);
                MPI.COMM_WORLD.Send(new Object[]{currentPuzzle}, 0, 1, MPI.OBJECT, i + 1, 0);
                MPI.COMM_WORLD.Send(new int[]{minimumSteps}, 0, 1, MPI.INT, i + 1, 0);
            }

            Object[] pairs = new Object[instancesCount + 5];
            // receive search state from workers
            for (int i = 1; i <= puzzlesQueue.size(); i++) {
                MPI.COMM_WORLD.Recv(pairs, i - 1, 1, MPI.OBJECT, i, 0);
            }

            // check if any worker found a solution
            int newMinimumSteps = Integer.MAX_VALUE;
            for (int i = 0; i < puzzlesQueue.size(); i++) {
                Pair<Integer, Puzzle> minStepsPuzzlePair = (Pair<Integer, Puzzle>) pairs[i];
                // if 0 steps, we found the solution
                if (minStepsPuzzlePair.getFirst() == 0) {
                    log.info("Solution found in " + minStepsPuzzlePair.getSecond().getStepsPerformed() + " steps");
                    log.info("Solution is: " + minStepsPuzzlePair.getSecond().toStringWithHistory());
                    log.info("Execution time: " + (System.currentTimeMillis() - time) + "ms");
                    solutionFound = true;
                    break;
                } else if (minStepsPuzzlePair.getFirst() < newMinimumSteps) {
                    newMinimumSteps = minStepsPuzzlePair.getFirst();
                }
            }

            if (!solutionFound) {
                log.info(newMinimumSteps + " minimum steps reached in " + (System.currentTimeMillis() - time) + "ms");
                minimumSteps = newMinimumSteps;
            }
        }

        for (int i = 1; i < instancesCount; i++) {
            // we signal workers that the solution was found so that they can exit
            Puzzle currentPuzzle = puzzlesQueue.poll();
            MPI.COMM_WORLD.Send(new boolean[]{true}, 0, 1, MPI.BOOLEAN, i, 0);
            MPI.COMM_WORLD.Send(new Object[]{currentPuzzle}, 0, 1, MPI.OBJECT, i, 0);
            MPI.COMM_WORLD.Send(new int[]{minimumSteps}, 0, 1, MPI.INT, i, 0);
        }
    }

    private static void workerLogic() {
        while (true) {
            Object[] puzzle = new Object[1];
            int[] minimumStepsResponse = new int[1];
            boolean[] solutionFound = new boolean[1];

            MPI.COMM_WORLD.Recv(solutionFound, 0, 1, MPI.BOOLEAN, 0, 0);
            MPI.COMM_WORLD.Recv(puzzle, 0, 1, MPI.OBJECT, 0, 0);
            MPI.COMM_WORLD.Recv(minimumStepsResponse, 0, 1, MPI.INT, 0, 0);

            // if master signaled that the solution was found, we end the execution
            if (solutionFound[0]) {
                log.info("Worker " + MPI.COMM_WORLD.Rank() + " is exiting ...");
                return;
            }

            int minimumSteps = minimumStepsResponse[0];
            Puzzle currentPuzzle = (Puzzle) puzzle[0];
            Pair<Integer, Puzzle> minStepsPuzzlePair = search(currentPuzzle, currentPuzzle.getStepsPerformed(), minimumSteps);
            MPI.COMM_WORLD.Send(new Object[]{minStepsPuzzlePair}, 0, 1, MPI.OBJECT, 0, 0);
        }
    }

    public static Pair<Integer, Puzzle> search(Puzzle puzzle, int stepsPerformed, int minimumSteps) {
        int stepsEstimation = stepsPerformed + puzzle.getManhattanDistance();
        if (stepsEstimation > minimumSteps) {
            return Pair.of(stepsEstimation, puzzle);
        }
        if (puzzle.getManhattanDistance() == 0) {
            return Pair.of(0, puzzle);
        }

        // we compute the best next move based on the minimum steps left of each generated move
        int newMinimumSteps = Integer.MAX_VALUE;
        Puzzle newPuzzle = null;
        for (Puzzle currentPuzzle : puzzle.generateValidMoves()) {
            Pair<Integer, Puzzle> result = search(currentPuzzle, stepsPerformed + 1, minimumSteps);
            int currentMinimumSteps = result.getFirst();
            if (currentMinimumSteps == 0) {
                return Pair.of(0, result.getSecond());
            }

            if (currentMinimumSteps < newMinimumSteps) {
                newMinimumSteps = currentMinimumSteps;
                newPuzzle = result.getSecond();
            }
        }

        return Pair.of(newMinimumSteps, newPuzzle);
    }
}
