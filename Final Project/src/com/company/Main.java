package com.company;

import mpi.MPI;

public class Main {
    public static void main(String[] args) {
        // write your code here
        MPI.Init(args);
        int me = MPI.COMM_WORLD.Rank();
        int size = MPI.COMM_WORLD.Size();
        System.out.println("Hello world from <" + me + "> of <" + size + ">");
        MPI.Finalize();
    }
}
